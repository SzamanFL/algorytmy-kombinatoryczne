#!/usr/bin/python3

def weight(x):
    # jesli parzysta liczba jedynek to zamieniamy ostatni bit
    if(sum(x) % 2 == 0):
        x[-1] = 1 if (x[-1] == 0) else 0
        convert(x)
        return True
    else:
        # wyznacz pierwsza jedynke od prawej strony i zamien na przeciwny nastepny bit    
        k = len(x) - 1
        while (k != -1):
            if (x[k] != 1):
                k -= 1
            else:
                if (k>0):
                    # zmienianie pierwszej jedynki od prawej na przeciwny znak
                    x[k-1] = 1 if (x[k-1] == 0) else 0
                    convert(x)
                    return True
                else:
                    # koniec
                    return False

# zamiana zbioru zer i jedynek na zbior licz naturalnych
def convert(x):
    n = len(x)
    result = []
    for i, value in enumerate(x):
        if(value != 0):
            result.append(i+1)
    print(result)

n = int(input("Podaj n: "))
series = [0 for i in range(n)]
checker = True
while checker:
    # zle bo odp ma byc w zbiorach liczb naturalnych a nie bitowych
    checker = weight(series)


    