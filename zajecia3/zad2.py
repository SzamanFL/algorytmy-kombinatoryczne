#!/usr/bin/python3
from operator import xor

def createZeroOneList(xlist, n):
    binCollection = [0 for i in range(n)]
    for value in xlist:
        binCollection[int(value)-1] = 1
    print(f"Zbiór przedstawiony za pomocą wektora charakterystycznego: {binCollection}")
    xorSeries(binCollection)

def xorSeries(series):
    binNumber = []
    start = 0
    for value in series:
        newNum = xor(start, value)
        binNumber.append(newNum)
        start = newNum
    print(f"Postać binarna rangi w postaci listy: {binNumber}")
    seriesToNum(binNumber)
    
def seriesToNum(series):
    num = ""
    for i in series:
        num += str(i)
    print(f"Ranga(liczona od 1): {int(num,2)}")

n = int(input("Podaj liczbę n: "))
series = input("Podaj liczby: ").split()
if(len(series) > n):
    print("Bad series")
else:
    createZeroOneList(series, n)