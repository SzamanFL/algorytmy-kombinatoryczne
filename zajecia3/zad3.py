#!/usr/bin/python3
from operator import xor

def getBinSeries(r):
    rBin = bin(r)[2:]
    rBinHalf = f"0{bin(int(r/2))[2:]}"
    series = []
    for i in range(len(str(rBin))):
        series.append(xor(int(rBin[i]), int(rBinHalf[i])))
    return series

def getNumSeries(binSeries, n):
    numSeries = []
    if(len(binSeries) > n):
        print("Bad r input")
        return False
    if(len(binSeries) < n):
        for j in range(n - len(binSeries)):
            binSeries.insert(0,0)
    for i in range(n):
        if(binSeries[i] == 1):
            numSeries.append(i+1)
    print(f"Series: {numSeries}")

n = int(input("Podaj n: "))
r = int(input("Podaj rangę(indeksowane od 1): "))

series = getBinSeries(r)
getNumSeries(series,n)