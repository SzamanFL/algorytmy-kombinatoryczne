#!/usr/bin/python3

n = int(input("podaj liczbę maksymalna(n): "))
series = [0 for i in range(pow(2,n))] # od 1 do k
metaseries = []
numseries = {}
last = []
for i in range(len(series)):
    num_bin = bin(i)[2:]
    for j in range(n-len(num_bin)):
        num_bin = '0' + num_bin
    series[i] = num_bin

for ele in series:
    num_bin = str(ele)
    nset = []
    for i in range(len(num_bin)):
        if(int(num_bin[i]) == 1):
            nset.append(i+1)
    metaseries.append(nset)

for index, element in enumerate(metaseries):
    numseries[index] = len(element)

for elem in sorted(numseries.items(), key=lambda x: x[1]):
    last.append(metaseries[elem[0]])
    
print(last)