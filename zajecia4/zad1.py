#!/usr/bin/python3

def SearchAndAdd(series, n, k):
    i = k
    while (series[i] == n-k+i):
        i=i-1
    if (i != 0):
        series[i] += 1
        for j in range(i+1, k+1):
            series[j] = series[j-1] + 1
    else:
        print("Nie ma nic dalej")
        return False
    del series[0]
    print(series)

n = int(input("Podaj n: "))
k = int(input("Podaj k: "))
series = input("Podaj elementy zbioru: ").split()
series = list(map(int, series))
series.insert(0,0)

if(len(series) != k+1):
    print("Zbyt duzy ciąg")
else:
    SearchAndAdd(series,n,k)